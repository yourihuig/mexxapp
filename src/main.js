/*
	Colors used:
		Panels: 		#fff9e3
		Green:        	#27ae60
		Dark-Green:   	#1e874a
		Orange: 		#f1c40f
		Dark-Orange: 	#f39c12
		Black: 			#3d3d3d
*/

let game

const gameOptions = {
	aspectRatio: 16/9,
	logoOffset: 120,
	titleOffset: 300,
	playerCountOffset: 200,
	buttonOffset: 120,
	hardcoreOffset: 360,
	footerOffset: 60,
	madeByOffset: 20,
	rulesOffset: 40,
	rulesTweenDuration: 200,
	rulesAlpha: 1,
	closeButtonOffset: 20,
	utilsSize: {
		width: 50,
		height: 50
	},
	diceSpacing: 300,
	diceSize: {
		width: 200,
		height: 200
	},
	shakeOffset: 30,
	shakeDuration: 20,
	shakeMinTimes: 4,
	iconsSize: {
		width: 200,
		height: 200
	},
	playerColumns: 2,
	playerColumnSpacing: 60,
	minPlayers: 2,
	maxPlayers: 10,
	messageOffset: 150,
	messageDuration: 300,
	messageTweenDuration: 200,
	messageAlpha: 1,
	nextPlayerDuration: 400,
	eventOffset: 150,
	eventDuration: 1000,
	eventTweenDuration: 200,
	eventAlpha: 1,
	eventPadding: 60,
	drinkOffset: 150,
	drinkTweenDuration: 200,
	drinkAlpha: 1,
	drinkPadding: 60,
	beerSize: {
		width: 130,
		height: 210
	},
	miniDiceTweenDuration: 100,
	vsTweenDuration: 100
}

window.onload = function() {
	let gameConfig = {
		width: 600,
		height: 600 * gameOptions.aspectRatio,
		backgroundColor: "#3d3d3d",
		scene: [boot, menu, playEasy, playHard]
	}
	game = new Phaser.Game(gameConfig);

	window.focus();
	resizeGame();
	window.addEventListener("resize", resizeGame);
}

function resizeGame() {
	let canvas = document.querySelector("canvas");
	let windowWidth = window.innerWidth;
	let windowHeight = window.innerHeight;
	let windowRatio = windowWidth / windowHeight;
	let gameRatio = game.config.width / game.config.height;
	if(windowRatio < gameRatio) {
		canvas.style.width = windowWidth + "px";
		canvas.style.height = (windowWidth / gameRatio) + "px";
	} else {
		canvas.style.width = (windowHeight * gameRatio) + "px";
		canvas.style.height = windowHeight + "px";
	}
}

class boot extends Phaser.Scene {
	constructor() {
		super("Boot")
	}

	preload() {
		this.load.spritesheet("dices", "./assets/sprites/dices.png",{
			frameWidth: gameOptions.diceSize.width,
			frameHeight: gameOptions.diceSize.height
		})
		this.load.image("moving_dice", "./assets/sprites/moving_dice.png");

		this.load.spritesheet("icons", "./assets/sprites/icons.png",{
			frameWidth: gameOptions.iconsSize.width,
			frameHeight: gameOptions.iconsSize.height
		});

		this.load.image("logo", "./assets/sprites/logo.png");
		this.load.image("start", "./assets/sprites/start.png");
		this.load.image("rules", "./assets/sprites/rules.png");

		this.load.image("hardcore", "./assets/sprites/hardcore.png");
		this.load.image("hardcore_left", "./assets/sprites/hardcore_left.png");
		this.load.image("hardcore_right", "./assets/sprites/hardcore_right.png");
		this.load.image("hardcore_middle", "./assets/sprites/hardcore_middle.png");

		this.load.image("panel", "./assets/sprites/panel.png");
		this.load.image("small_panel", "./assets/sprites/small_panel.png");
		this.load.image("big_panel", "./assets/sprites/big_panel.png");
		
		this.load.image("close", "./assets/sprites/close.png");

		this.load.spritesheet("utils", "./assets/sprites/utils.png",{
			frameWidth: gameOptions.utilsSize.width,
			frameHeight: gameOptions.utilsSize.height
		})

		this.load.spritesheet("beer_loader", "./assets/sprites/beer_loader.png",{
			frameWidth: gameOptions.beerSize.width,
			frameHeight: gameOptions.beerSize.height
		})

		this.load.bitmapFont("coolvetica", "./assets/fonts/coolvetica.png", "./assets/fonts/coolvetica.fnt");
		this.load.bitmapFont("coolvetica_small_green", "./assets/fonts/coolvetica_small_green.png", "./assets/fonts/coolvetica_small_green.fnt");
		this.load.bitmapFont("coolvetica_medium", "./assets/fonts/coolvetica_medium.png", "./assets/fonts/coolvetica_medium.fnt");
		this.load.bitmapFont("coolvetica_dark", "./assets/fonts/coolvetica_dark.png", "./assets/fonts/coolvetica_dark.fnt");
	}

	create() {
		this.scene.start("Menu");
	}
}

class menu extends Phaser.Scene {
	constructor() {
		super("Menu");
	}

	create() {
		let middleXY = new Phaser.Geom.Point(game.config.width / 2, gameOptions.logoOffset);

		let logo = this.add.image(middleXY.x, middleXY.y, "logo");
		logo.setScale(0.8);

		let title = this.add.bitmapText(middleXY.x, gameOptions.titleOffset, "coolvetica", "Mexx App");
		title.setOrigin(0.5, 0.5);

		let start = this.add.image(middleXY.x, (game.config.height / 2) - gameOptions.buttonOffset, "start");
		start.setInteractive();
		start.on("pointerdown", function(){
			if(!rulesPanel.visible) {
				this.tweens.add({
					targets: [start],
					scaleX: 1.1,
					scaleY: 1.1,
					duration: 100,
					yoyo: true,
					callbackScope: this,
					onComplete: function() {
						this.scene.start("PlayEasy", {playerAmmount});
					}
 				});
			}
		}, this);

		let rulesPanel = this.add.image(middleXY.x, game.config.height / 2, "big_panel");
		rulesPanel.alpha = 0;
		rulesPanel.depth = 10;
		rulesPanel.visible = false;

		let closeButton = this.add.image(middleXY.x + (rulesPanel.width / 2) - gameOptions.closeButtonOffset, (game.config.height / 2) - (rulesPanel.height / 2) + gameOptions.closeButtonOffset, "close");
		closeButton.alpha = 0;
		closeButton.depth = 15;
		closeButton.visible = false;
		closeButton.setInteractive();
		closeButton.on("pointerdown", function(){
			if(rulesPanel.visible) {
				this.tweens.add({
					targets: [rulesPanel, closeButton],
					alpha: 0,
					duration: gameOptions.rulesTweenDuration,
					callbackScope: this,
					onComplete: function(){
						rulesPanel.visible = false;
						closeButton.visible = false;
					}
				})
			}
		}, this);

		let rules = this.add.image(middleXY.x, game.config.height / 2, "rules");
		rules.setInteractive();
		rules.on("pointerdown", function(){
			if(!rulesPanel.visible) {
				this.tweens.add({
					targets: [rules],
					scaleX: 1.1,
					scaleY: 1.1,
					duration: 100,
					yoyo: true,
					callbackScope: this,
					onComplete: function() {
						rulesPanel.visible = true;
						closeButton.visible = true;
			  			this.tweens.add({
							targets: [rulesPanel, closeButton],
							alpha: 1,
							duration: gameOptions.rulesTweenDuration,
							callbackScope: this
						})
					}
 				});
  			}
		}, this);

		let playerCountLabel = this.add.bitmapText(middleXY.x, (game.config.height / 2) + gameOptions.playerCountOffset - 80, "coolvetica_medium", "Spelers");
		playerCountLabel.setOrigin(0.5, 0.4);

		let playerAmmount = 4;
		let playerCount = this.add.bitmapText(middleXY.x, (game.config.height / 2) + gameOptions.playerCountOffset, "coolvetica_medium", playerAmmount.toString());
		playerCount.setOrigin(0.5, 0.4);

		let playerPlus = this.add.sprite(middleXY.x + 80, (game.config.height / 2) + gameOptions.playerCountOffset, "utils", 1);
		playerPlus.setInteractive();
		playerPlus.on("pointerdown", function(){
			if(!rulesPanel.visible && playerAmmount != gameOptions.maxPlayers) {
				playerAmmount++;
				playerCount.text = playerAmmount.toString();
			}
		}, this);
		let playerMinus = this.add.sprite(middleXY.x - 80, (game.config.height / 2) + gameOptions.playerCountOffset, "utils", 2);
		playerMinus.setInteractive();
		playerMinus.on("pointerdown", function(){
			if(!rulesPanel.visible && playerAmmount != gameOptions.minPlayers) {
				playerAmmount--;
				playerCount.text = playerAmmount.toString();
			}
		}, this);

		let startHardcore = this.add.image(middleXY.x, game.config.height / 2 + gameOptions.hardcoreOffset, "hardcore");
		startHardcore.setInteractive();
		startHardcore.on("pointerdown", function(){
			if(!rulesPanel.visible) {
				this.tweens.add({
					targets: [startHardcore],
					scaleX: 1.1,
					scaleY: 1.1,
					duration: 100,
					yoyo: true,
					callbackScope: this,
					onComplete: function() {
						this.scene.start("PlayHard");
					}
 				});
			}
		}, this);

		let footer = this.add.bitmapText(middleXY.x, game.config.height - gameOptions.footerOffset, "coolvetica_small_green", "Veel plezier op de introweek!");
		footer.setOrigin(0.5, 0.5);

		let madeBy = this.add.bitmapText(middleXY.x, game.config.height - gameOptions.madeByOffset, "coolvetica_small_green", "Made by Youri Huig");
		madeBy.setOrigin(0.5, 0.5);
	}
}

class playEasy extends Phaser.Scene {
	constructor() {
		super("PlayEasy");
	}

	create(data) {
		this.playerAmmount = data.playerAmmount;

		this.loserArray = [];

		this.currentPlayer = 0;
		this.maxTurns = 3;

		this.lowestRoll = 600;

		this.firstRound = true;

		this.gamePhase = 0;
		this.gamePhaseInProgress = false;

		this.space = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);

		let rollButton = new Phaser.Geom.Rectangle(50, (game.config.height / 2) - 100, 500, 200);

		this.input.on("pointerdown", function(){
			if(rollButton.contains(this.input.activePointer.x, this.input.activePointer.y)) {
				this.pointerDown = true;
			}
		}, this);

		this.input.on("pointerup", function(){
			this.pointerDown = false;
		}, this);

		let middleXY = new Phaser.Geom.Point(game.config.width / 2, gameOptions.logoOffset);

		let logo = this.add.image(middleXY.x, middleXY.y, "logo");
		logo.setScale(0.8);

		let title = this.add.bitmapText(middleXY.x, gameOptions.titleOffset, "coolvetica", "Mexx App");
		title.setOrigin(0.5, 0.5);

		let footer = this.add.bitmapText(middleXY.x, game.config.height - gameOptions.footerOffset, "coolvetica_small_green", "Veel plezier op de introweek!");
		footer.setOrigin(0.5, 0.5);

		let madeBy = this.add.bitmapText(middleXY.x, game.config.height - gameOptions.madeByOffset, "coolvetica_small_green", "Made by Youri Huig");
		madeBy.setOrigin(0.5, 0.5);

		this.messageXY = new Phaser.Geom.Point(game.config.width / 2, (game.config.height / 2) - gameOptions.messageOffset);

		this.messagePanel = this.add.image(this.messageXY.x, this.messageXY.y, "small_panel");
		this.messagePanel.depth = 10;
		this.messagePanel.alpha = 0;
		this.messagePanel.visible = false;

		this.messageText = this.add.bitmapText(this.messageXY.x, this.messageXY.y, "coolvetica_dark", "Message");
		this.messageText.depth = 15;
		this.messageText.alpha = 0;
		this.messageText.visible = false;
		this.messageText.setOrigin(0.5, 0.4);

		this.eventXY = new Phaser.Geom.Point(game.config.width / 2, (game.config.height / 2) - gameOptions.eventOffset);

		this.eventPanel = this.add.image(this.eventXY.x, this.eventXY.y, "panel");
		this.eventPanel.depth = 10;
		this.eventPanel.alpha = 0;
		this.eventPanel.visible = false;

		this.eventSprite = this.add.sprite(this.eventXY.x, this.eventXY.y, "icons", 0)
		this.eventSprite.depth = 15;
		this.eventSprite.alpha = 0;
		this.eventSprite.visible = false;
		this.eventSprite.setOrigin(0.5, 0.8);

		this.eventText = this.add.bitmapText(this.eventXY.x, this.eventXY.y, "coolvetica_dark", "Message");
		this.eventText.depth = 15;
		this.eventText.alpha = 0;
		this.eventText.visible = false;
		this.eventText.setOrigin(0.5, -0.8);

		this.drinkXY = new Phaser.Geom.Point(game.config.width / 2, (game.config.height / 2) - gameOptions.eventOffset);

		this.drinkPanel = this.add.image(this.drinkXY.x, this.drinkXY.y, "panel");
		this.drinkPanel.depth = 10;
		this.drinkPanel.alpha = 0;
		this.drinkPanel.visible = false;

		this.drinkSprite = this.add.sprite(this.drinkXY.x, this.drinkXY.y, "beer_loader", 0)
		this.drinkSprite.depth = 15;
		this.drinkSprite.alpha = 0;
		this.drinkSprite.visible = false;
		this.drinkSprite.setScale(0.8);
		this.drinkSprite.setOrigin(0.5, 1.1);

		this.drinkPlayer = this.add.bitmapText(this.drinkXY.x, this.drinkXY.y, "coolvetica_dark", "Speler 1");
		this.drinkPlayer.depth = 15;
		this.drinkPlayer.alpha = 0;
		this.drinkPlayer.visible = false;
		this.drinkPlayer.setOrigin(0.5, 0);

		this.drinkText = this.add.bitmapText(this.drinkXY.x, this.drinkXY.y, "coolvetica_dark", "Moet drinken!");
		this.drinkText.depth = 15;
		this.drinkText.alpha = 0;
		this.drinkText.visible = false;
		this.drinkText.setOrigin(0.5, -1);

		this.drinkPanel.setInteractive();
		this.drinkPanel.on("pointerdown", function(){
			let newDrinkXY = new Phaser.Geom.Point(this.drinkXY.x, this.drinkXY.y);
			if(this.drinkPanel.alpha == 1) {
				this.tweens.add({
					targets: [this.drinkPanel, this.drinkSprite, this.drinkPlayer, this.drinkText],
					x: newDrinkXY.x,
					y: newDrinkXY.y += gameOptions.drinkOffset * 2,
					alpha: 0,
					duration: gameOptions.drinkTweenDuration,
					ease: 'Power1',
					callbackScope: this,
					onComplete: function () {
						this.drinkPanel.visible = false;
						this.drinkPanel.setPosition(this.drinkXY.x, this.drinkXY.y);

						this.drinkSprite.visible = false;
						this.drinkSprite.setPosition(this.drinkXY.x, this.drinkXY.y);

						this.drinkPlayer.visible = false;
						this.drinkPlayer.setPosition(this.drinkXY.x, this.drinkXY.y);

						this.drinkText.visible = false;
						this.drinkText.setPosition(this.drinkXY.x, this.drinkXY.y);

						this.resetGame();
					}
				})
			}
		}, this);

		this.diceArray = [];

		this.diceArray.push({
			isOneOrTwo: false,
			value: 0,
			diceSprite: this.add.sprite(gameOptions.diceSpacing / 2, game.config.height / 2, "dices", 0),
			movingDiceImage: this.add.image(gameOptions.diceSpacing / 2, game.config.height / 2, "moving_dice")
		});
		this.diceArray.push({
			isOneOrTwo: false,
			value: 1,
			diceSprite: this.add.sprite((gameOptions.diceSpacing / 2) + gameOptions.diceSpacing, game.config.height / 2, "dices", 1),
			movingDiceImage: this.add.image((gameOptions.diceSpacing / 2) + gameOptions.diceSpacing, game.config.height / 2, "moving_dice")
		});

		for(let i = 0; i < this.diceArray.length; ++i) {
			this.diceArray[i].movingDiceImage.visible = false;
		}

		this.vsPlayerArray = [];

		this.vsPlayerArray.push(
			this.add.bitmapText(gameOptions.diceSpacing / 2, (game.config.height / 2) - 150, "coolvetica_medium", "P1")
		);
		this.vsPlayerArray.push(
			this.add.bitmapText((gameOptions.diceSpacing / 2) + gameOptions.diceSpacing, (game.config.height / 2) - 150, "coolvetica_medium", "P2")
		);

		this.vsText = this.add.bitmapText(game.config.width / 2, (game.config.height / 2) - 150, "coolvetica_medium", "VS")
		this.vsText.visible = false;
		this.vsText.setOrigin(0.5, 0.5);

		for(let i = 0; i < this.vsPlayerArray.length; ++i) {
			this.vsPlayerArray[i].depth = 15;
			this.vsPlayerArray[i].visible = false;
			this.vsPlayerArray[i].setOrigin(0.5, 0.5);
		}

		this.playerArray = [];
		this.losingPlayerArray = [];

		let playerCount = 0;
		let rows = (this.playerAmmount + ((Math.abs(this.playerAmmount % 2) == 1) ? 1 : 0)) / gameOptions.playerColumns;	

		for (let col = 0; col < gameOptions.playerColumns; col++) {
			for(let row = 0; row < rows; row++) {
				if(playerCount < this.playerAmmount) {
					let posX = gameOptions.diceSpacing * col;
					let offsetX = (gameOptions.diceSpacing / 2) - 50;
					posX+= offsetX;

					let posY = gameOptions.playerColumnSpacing * row;
					let offsetY = game.config.height - (gameOptions.playerColumnSpacing * rows) - gameOptions.playerColumnSpacing;
					posY+= offsetY;

					let miniDices = [];
					// Add mini score dices based on the ammount of dices on the board.
					for(let i = 0; i < this.diceArray.length; ++i) {
						let miniDiceSprite = this.add.sprite(posX + (50 * i) + 60, posY, "dices", 0);
						miniDiceSprite.setScale(0.2);
						miniDiceSprite.visible = false;
						miniDices.push(miniDiceSprite);
					}

					let playerLabel = this.add.bitmapText(posX, posY, "coolvetica_medium", "P" + (playerCount + 1).toString())
					playerLabel.setOrigin(0.5,0.35);

					this.playerArray.push({
						total: 0,
						turn: 0,
						label: playerLabel,
						dices: miniDices
					});

					if(playerCount == this.currentPlayer) {
						this.playerArrow = this.add.sprite(posX - 50, posY, "utils", 0);
					}

					playerCount++;
				}
			}
		}
	}

	update(time, delta) {
		if(!this.gamePhaseInProgress) {
			switch(this.gamePhase) {
				case 0:
					if (this.space.isDown) {
						this.gamePhaseInProgress = true;

						this.rollingDices = 0;
						for(let i = 0; i < this.diceArray.length; ++i) {
							if(!this.diceArray[i].isOneOrTwo) {
								this.rollingDices++;
								this.rollDice(this.diceArray[i]);
							}
						}
					}
					if (this.pointerDown && !this.drinkPanel.visible) {
						this.gamePhaseInProgress = true;

						this.rollingDices = 0;
						for(let i = 0; i < this.diceArray.length; ++i) {
							if(!this.diceArray[i].isOneOrTwo) {
								this.rollingDices++;
								this.rollDice(this.diceArray[i]);
							}
						}
					}
					break;
				case 1:
					this.gamePhaseInProgress = true;
					this.countDice();
					break;
				case 2:
					this.gamePhaseInProgress = true;
					this.checkScores();
					break;
				case 3:
					this.gamePhaseInProgress = true;
					this.finishGame();
					break;
				case 4:
					if (this.space.isDown) {
						this.gamePhaseInProgress = true;

						this.rollingDices = 0;
						for(let i = 0; i < this.diceArray.length; ++i) {
							this.rollingDices++;
							this.rollDice(this.diceArray[i]);
						}
					}
					if (this.pointerDown && !this.drinkPanel.visible) {
						this.gamePhaseInProgress = true;

						this.rollingDices = 0;
						for(let i = 0; i < this.diceArray.length; ++i) {
							this.rollingDices++;
							this.rollDice(this.diceArray[i]);
						}
					}
					break;
				case 5:
					this.gamePhaseInProgress = true;

					if(this.diceArray[0].value > this.diceArray[1].value) {
						this.loserArray.splice(0, 1);
					}
					if(this.diceArray[0].value < this.diceArray[1].value) {
						this.loserArray.splice(1, 1);
					}

					this.gamePhase = 3;
					this.gamePhaseInProgress = false;
					break;
			}
		}
	}

	rollDice(dice, offset = gameOptions.shakeOffset, times = 0) {
		dice.diceSprite.visible = false;
		dice.movingDiceImage.visible = true;
		
		this.tweens.add({
			targets: [dice.movingDiceImage],
			x: dice.movingDiceImage.x,
			y: dice.movingDiceImage.y - offset,
			yoyo: true,
			duration: gameOptions.shakeDuration,
			callbackScope: this,
			onComplete: function(){
				if(this.space.isDown || this.pointerDown || times < gameOptions.shakeMinTimes) {
					this.rollDice(dice, -offset, times + 1)
				} else {
					dice.diceSprite.visible = true;
					dice.movingDiceImage.visible = false;

					let random = Phaser.Math.Between(0, 5);
					dice.value = random;
					dice.diceSprite.setFrame(random);

					if(random == 0 || random == 1) {
						dice.isOneOrTwo = true;
					}

					this.rollingDices --;
					if(this.rollingDices == 0) {
						this.gamePhase++;
						this.gamePhaseInProgress = false;
					}
				}
			}
		})
	}

	countDice() {
		let total = "";
		if(this.diceArray[0].value < this.diceArray[1].value) {
			total = (this.diceArray[1].value + 1).toString() + (this.diceArray[0].value + 1).toString();
		}
		if(this.diceArray[0].value > this.diceArray[1].value) {
			total = (this.diceArray[0].value + 1).toString() + (this.diceArray[1].value + 1).toString();
		}
		if(this.diceArray[0].value == this.diceArray[1].value) {
			total = (this.diceArray[0].value + 1).toString() + "00";
			if(this.diceArray[0].value == 0 || this.diceArray[0].value == 1) {
				this.diceArray[1].isOneOrTwo = false;
			}
		}
		this.playerArray[this.currentPlayer].total = parseInt(total);

		switch(this.playerArray[this.currentPlayer].total) {
			case 21:
				this.playerArray[this.currentPlayer].turn++;
				if(this.firstRound)
					this.maxTurns = this.playerArray[this.currentPlayer].turn;
				this.playerArray[this.currentPlayer].turn = this.maxTurns;
				this.showEvent("Mexx!", 0);
				break;
			case 32:
				this.playerArray[this.currentPlayer].turn++;
				if(this.firstRound)
					this.maxTurns = this.playerArray[this.currentPlayer].turn;
				this.playerArray[this.currentPlayer].turn = this.maxTurns;
				this.showEvent("Laagst!", 1);
				break;
			case 31:
				this.resetDice();
				this.showEvent("Aanwijzen!", 2);
				break;
			case 500:
				this.playerArray[this.currentPlayer].turn++;
				this.showEvent("Duimen!", 3);
				break;
			case 600:
				this.showEvent("Zelf drinken!", 4);
				break;
			default:
				this.playerArray[this.currentPlayer].turn++;
				this.showMessage(this.playerArray[this.currentPlayer].total.toString());
				break;
		}
	}

	checkScores() {
		for(let i = 0; i < this.playerArray[this.currentPlayer].dices.length; ++i) {
			this.playerArray[this.currentPlayer].dices[i].setFrame(this.diceArray[i].value);
			this.playerArray[this.currentPlayer].dices[i].visible = true;
		}

		this.tweens.add({
			targets: this.playerArray[this.currentPlayer].dices,
			scaleX: 0.25,
			scaleY: 0.25,
			duration: gameOptions.miniDiceTweenDuration,
			yoyo: true,
			repeat: 1,
			callbackScope: this
		});

		if(this.playerArray[this.currentPlayer].turn == this.maxTurns) {
			this.firstRound = false;

			if(this.playerArray[this.currentPlayer].total < this.lowestRoll && this.playerArray[this.currentPlayer].total != 21) {
				this.lowestRoll = this.playerArray[this.currentPlayer].total;
			}

			// Set next player
			this.currentPlayer = (this.currentPlayer == this.playerAmmount - 1) ? 0 : this.currentPlayer + 1;

			// Check if next player has turns left else the game is finished
			if(this.playerArray[this.currentPlayer].turn == this.maxTurns) {
				for(let playerIndex = 0; playerIndex < this.playerArray.length; ++playerIndex) {
					if(this.playerArray[playerIndex].total == this.lowestRoll) {
						this.loserArray.push(playerIndex);
					}
				}

				this.gamePhase++;
				this.gamePhaseInProgress = false;
			} else {			
				this.resetDice();

				this.tweens.add({
					targets: [this.playerArrow],
					x: this.playerArray[this.currentPlayer].label.x - 50,
					y: this.playerArray[this.currentPlayer].label.y,
					duration: gameOptions.messageTweenDuration,
					ease: 'Power1',
					callbackScope: this
				});

				this.showMessage("Speler " + (this.currentPlayer + 1) + " is!", true);
			}
		} else {
			this.gamePhase = 0;
			this.gamePhaseInProgress = false;
		}
	}

	finishGame() {
		if(this.loserArray.length == 1) {
			this.playerArrow.visible = true;
			this.vsPlayerArray[0].visible = false;
			this.vsPlayerArray[1].visible = false;
			this.vsText.visible = false;

			// Loser begins next round
			this.currentPlayer = this.loserArray[0];

			this.showDrink(this.loserArray[0])
		} else {
			this.resetDice();

			this.playerArrow.visible = false;
			this.vsPlayerArray[0].visible = true;
			this.vsPlayerArray[0].text = "P" + this.loserArray[0].toString();
			this.vsPlayerArray[1].visible = true;
			this.vsPlayerArray[1].text = "P" + this.loserArray[1].toString();
			this.vsText.visible = true;

			this.tweens.add({
				targets: [this.vsPlayerArray[0], this.vsPlayerArray[1], this.vsText],
				scaleX: 0.25,
				scaleY: 0.25,
				duration: gameOptions.vsTweenDuration,
				yoyo: true,
				repeat: 1,
				callbackScope: this,
				onComplete: function () {
					this.showMessage("Hoogste wint!");
				}
			})
		}
	}

	resetGame() {
		this.resetDice();
		this.loserArray = [];
		this.maxTurns = 3;
		this.lowestRoll = 600;
		this.firstRound = true;

		for(let playerIndex = 0; playerIndex < this.playerArray.length; ++playerIndex) {
			this.playerArray[playerIndex].total = 0;
			this.playerArray[playerIndex].turn = 0;
			for(let i = 0; i < this.playerArray[playerIndex].dices.length; ++i) {
				this.playerArray[playerIndex].dices[i].setFrame(0);
				this.playerArray[playerIndex].dices[i].visible = false;
			}
		}

		this.tweens.add({
			targets: [this.playerArrow],
			x: this.playerArray[this.currentPlayer].label.x - 50,
			y: this.playerArray[this.currentPlayer].label.y,
			duration: gameOptions.messageTweenDuration,
			ease: 'Power1',
			callbackScope: this
		});

		this.showMessage("Speler " + (this.currentPlayer + 1) + " is!", true);
	}

	showMessage(text, isNextPlayer = false) {
		this.messagePanel.visible = true;
		this.messageText.text = text;
		this.messageText.visible = true;

		let newMessageXY = new Phaser.Geom.Point(this.messageXY.x, this.messageXY.y);

		this.tweens.add({
			targets: [this.messagePanel, this.messageText],
			x: newMessageXY.x,
			y: newMessageXY.y += gameOptions.messageOffset,
			alpha: gameOptions.messageAlpha,
			duration: gameOptions.messageTweenDuration,
			ease: 'Power1',
			completeDelay: (isNextPlayer) ? gameOptions.nextPlayerDuration : gameOptions.messageDuration,
			callbackScope: this,
			onComplete: function(){	
				this.tweens.add({
					targets: [this.messagePanel, this.messageText],
					x: newMessageXY.x,
					y: newMessageXY.y += gameOptions.messageOffset,
					alpha: 0,
					duration: gameOptions.messageTweenDuration,
					ease: 'Power1',
					callbackScope: this,
					onComplete: function(){	
						this.messagePanel.visible = false;
						this.messagePanel.setPosition(this.messageXY.x, this.messageXY.y);

						this.messageText.visible = false;
						this.messageText.setPosition(this.messageXY.x, this.messageXY.y);

						if(isNextPlayer) {
							this.gamePhase = 0;
						} else {
							this.gamePhase++;
						}
						this.gamePhaseInProgress = false;
					}
				})
			}
		})
	}

	showEvent(text, frame = 0) {
		this.eventPanel.visible = true;
		this.eventText.text = text;
		this.eventText.visible = true;
		this.eventSprite.setFrame(frame);
		this.eventSprite.visible = true;

		let newEventXY = new Phaser.Geom.Point(this.eventXY.x, this.eventXY.y);

		this.tweens.add({
			targets: [this.eventPanel, this.eventText, this.eventSprite],
			x: newEventXY.x,
			y: newEventXY.y += gameOptions.eventOffset,
			alpha: gameOptions.eventAlpha,
			duration: gameOptions.eventTweenDuration,
			completeDelay: gameOptions.eventDuration,
			callbackScope: this,
			onComplete: function(){	
				this.tweens.add({
					targets: [this.eventPanel, this.eventText, this.eventSprite],
					x: newEventXY.x,
					y: newEventXY.y += gameOptions.eventOffset,
					alpha: 0,
					duration: gameOptions.eventTweenDuration,
					ease: 'Power1',
					callbackScope: this,
					onComplete: function(){	
						this.eventPanel.visible = false;
						this.eventPanel.setPosition(this.eventXY.x, this.eventXY.y);

						this.eventText.visible = false;
						this.eventText.setPosition(this.eventXY.x, this.eventXY.y);

						this.eventSprite.visible = false;
						this.eventSprite.setPosition(this.eventXY.x, this.eventXY.y);

						this.gamePhase++;
						this.gamePhaseInProgress = false;
					}
				})
			}
		})
	}

	showDrink(player) {
		this.drinkPanel.visible = true;
		this.drinkSprite.visible = true;
		this.drinkPlayer.text = "Speler " + (player + 1).toString();
		this.drinkPlayer.visible = true;

		let random = Phaser.Math.Between(0, 1);
		this.drinkText.text = (random == 1) ? "Moet drinken!" : "Drinken gauw!";
		this.drinkText.visible = true;

		let newDrinkXY = new Phaser.Geom.Point(this.drinkXY.x, this.drinkXY.y);

		this.tweens.add({
			targets: [this.drinkPanel, this.drinkSprite, this.drinkPlayer, this.drinkText],
			x: newDrinkXY.x,
			y: newDrinkXY.y += gameOptions.drinkOffset,
			alpha: gameOptions.drinkAlpha,
			duration: gameOptions.drinkTweenDuration,
			ease: 'Power1',
			callbackScope: this
		})
	}

	resetDice() {
		for(let i = 0; i < this.diceArray.length; ++i) {
			this.diceArray[i].isOneOrTwo = false;
		}
	}
}

class playHard extends Phaser.Scene {
	constructor() {
		super("PlayHard");
	}

	create(data) {
		this.gamePhase = 0;
		this.gamePhaseInProgress = false;

		let middleXY = new Phaser.Geom.Point(game.config.width / 2, gameOptions.logoOffset);

		let logo = this.add.image(middleXY.x, middleXY.y, "logo");
		logo.setScale(0.8);

		let title = this.add.bitmapText(middleXY.x, gameOptions.titleOffset, "coolvetica", "Mexx App");
		title.setOrigin(0.5, 0.5);

		let footer = this.add.bitmapText(middleXY.x, game.config.height - gameOptions.footerOffset, "coolvetica_small_green", "Veel plezier op de introweek!");
		footer.setOrigin(0.5, 0.5);

		let madeBy = this.add.bitmapText(middleXY.x, game.config.height - gameOptions.madeByOffset, "coolvetica_small_green", "Made by Youri Huig");
		madeBy.setOrigin(0.5, 0.5);

		this.diceArray = [];

		this.diceArray.push({
			rollButton: new Phaser.Geom.Rectangle((gameOptions.diceSpacing / 2) - 100, (game.config.height / 2) - 100, 200, 200),
			clickable: true,
			diceSprite: this.add.sprite(gameOptions.diceSpacing / 2, game.config.height / 2, "dices", 0),
			movingDiceImage: this.add.image(gameOptions.diceSpacing / 2, game.config.height / 2, "moving_dice")
		});
		this.diceArray.push({
			rollButton: new Phaser.Geom.Rectangle(((gameOptions.diceSpacing / 2) + gameOptions.diceSpacing) - 100, (game.config.height / 2) - 100, 200, 200),
			clickable: true,
			diceSprite: this.add.sprite((gameOptions.diceSpacing / 2) + gameOptions.diceSpacing, game.config.height / 2, "dices", 1),
			movingDiceImage: this.add.image((gameOptions.diceSpacing / 2) + gameOptions.diceSpacing, game.config.height / 2, "moving_dice")
		});

		let hardcore_middle = this.add.image(middleXY.x, game.config.height - 200, "hardcore_middle");
		hardcore_middle.setInteractive();
		hardcore_middle.on("pointerdown", function(pointer){
			for(let i = 0; i < this.diceArray.length; ++i) {
				if(this.diceArray[i].clickable) {
					this.rollDice(this.diceArray[i], pointer);
				}
			}
		}, this);

		let hardcore_left = this.add.image(middleXY.x - (hardcore_middle.width / 2) - 50, game.config.height - 200, "hardcore_left");
		hardcore_left.setInteractive();
		hardcore_left.on("pointerdown", function(pointer){
			if(this.diceArray[0].clickable) {
				this.rollDice(this.diceArray[0], pointer);
			}
		}, this);

		let hardcore_right = this.add.image(middleXY.x + (hardcore_middle.width / 2) + 50, game.config.height - 200, "hardcore_right");
		hardcore_right.setInteractive();
		hardcore_right.on("pointerdown", function(pointer){
			if(this.diceArray[1].clickable) {
				this.rollDice(this.diceArray[1], pointer);
			}
		}, this);


		this.input.on('pointerdown', function(pointer){
			if(this.diceArray[0].rollButton.contains(pointer.x, pointer.y) && this.diceArray[0].clickable) {
				this.rollDice(this.diceArray[0], pointer);
			}
			if(this.diceArray[1].rollButton.contains(pointer.x, pointer.y) && this.diceArray[1].clickable) {
				this.rollDice(this.diceArray[1], pointer);
			}
		}, this);

		for(let i = 0; i < this.diceArray.length; ++i) {
			this.diceArray[i].movingDiceImage.visible = false;
		}
	}

	rollDice(dice, pointer, offset = gameOptions.shakeOffset, times = 0) {
		dice.diceSprite.visible = false;
		dice.movingDiceImage.visible = true;
		dice.clickable = false;

		this.tweens.add({
			targets: [dice.movingDiceImage],
			x: dice.movingDiceImage.x,
			y: dice.movingDiceImage.y - offset,
			yoyo: true,
			duration: gameOptions.shakeDuration,
			callbackScope: this,
			onComplete: function(){
				if(pointer.isDown || times < gameOptions.shakeMinTimes) {
					this.rollDice(dice, pointer, -offset, times + 1)
				} else {
					dice.diceSprite.visible = true;
					dice.movingDiceImage.visible = false;

					let random = Phaser.Math.Between(0, 5);
					dice.value = random;
					dice.diceSprite.setFrame(random);
					dice.clickable = true;
				}
			}
		})
	}
}